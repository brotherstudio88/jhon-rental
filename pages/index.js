import Navbar from './component/Navbar'
import Image from 'next/image'
import { useRouter } from 'next/router'
export default function Home() {
  const router = useRouter()
  return (
    <>
   <Navbar />
    <div className='header-wraper'>
      <div className='container'>
        <div className='row'>
          <div className='col-sm-12 col-md-6 col-lg-6'>
            <div className='colom-judul'>
              <h1 className='text-judul'><span className='text-span'>Jhon.</span></h1>
              <h1 className='text-judul'>Rental Mobil</h1>
              <p className='col-md-8 col-lg-8 text-sub-judul'>
                Rental Mobil Berkelas Hanya Di Jhon Rental,
                Berbagai Macam Pilihan Mobil Tersedia Disini. Anda Dapat Memilih Mobil Sesuai Keinginan
                </p>
                <button className='button-header' onClick={() => router.push('/rental')}>Rental Disini</button>
            </div>
          </div>
          <div className='col-sm-12 col-md-6 col-lg-6'>
            <div className='image-header d-md-block d-none'>
            <Image src='/image/alphard.png' width={800} height={450} />
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className='container'>
      <div className='col-sm-12 col-md-12 col-lg-12'>
        <div className='after-header-wraper'>
          <h1 className='text-after-header'>Deretan Mobil</h1>
          <p className='text-sub-after-header'>Terdiri Dari Berbagai Macam Mobil</p>
        </div>
      </div>
      <div className='row'>
        <div className='col-sm-6 col-md-4 col-lg-4'>
          <div className='card-jenis-mobil-wraper mb-4'>
            <div className='card-jenis-mobil'>
              <div className='image-card-wraper'>
                <Image src='/image/pajero.png' width={550} height={300} />
              </div>
            </div>
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-lg-4'>
          <div className='card-jenis-mobil-wraper mb-4'>
            <div className='card-jenis-mobil'>
              <div className='image-card-wraper'>
                <Image src='/image/expander.png' width={550} height={300} />
              </div>
            </div>
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-lg-4'>
          <div className='card-jenis-mobil-wraper mb-4'>
            <div className='card-jenis-mobil'>
              <div className='image-card-wraper'>
                <Image src='/image/pajero.png' width={550} height={300} />
              </div>
            </div>
          </div>
        </div>
        <div className='col-sm-6 col-md-4 col-lg-4'>
          <div className='card-jenis-mobil-wraper mb-4'>
            <div className='card-jenis-mobil'>
              <div className='image-card-wraper'>
                <Image src='/image/expander.png' width={550} height={300} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className='container'>
      <div className='col-sm-12 col-md-12 col-lg-12'>
        <div className='tentang-kami-wraper'>
          <h1 className='text-tentang-kami'>Tentang Kami</h1>
          <p className='text-after-judul-tentang-kami'>Ini Adalah Sekilas Tentang Kami</p>
        </div>
      </div>
      <div className='row'>
        <div className='col-sm-12 col-md-6 col-lg-6'>
            <div className='image-tentang-kami'>
              <Image src='/image/alphard.png' width={600} height={350} />
            </div>
        </div>
        <div className='col-sm-6 col-md-5 col-lg-5'>
          <h5 className='text-sub-tentang-kami'>
                Rental Mobil Berkelas Hanya Di Jhon Rental,
                Berbagai Macam Pilihan Mobil Tersedia Disini. Anda Dapat Memilih Mobil Sesuai Keinginan
                Rental Mobil Berkelas Hanya Di Jhon Rental,
                Berbagai Macam Pilihan Mobil Tersedia Disini. Anda Dapat Memilih Mobil Sesuai Keinginan
                Rental Mobil Berkelas Hanya Di Jhon Rental,
                Berbagai Macam Pilihan Mobil Tersedia Disini. Anda Dapat Memilih Mobil Sesuai Keinginan
                Rental Mobil Berkelas Hanya Di Jhon Rental,
                Berbagai Macam Pilihan Mobil Tersedia Disini. Anda Dapat Memilih Mobil Sesuai Keinginan
          </h5>
        </div>
      </div>
    </div>
    <div className='footer-wraper'>
      <div className='container'>
        <div className='text-footer-wraper'>
          <div className='row'>
            <div className='col-sm-4 col-md-3 col-lg-3'>
              <h1 className='text-footer-judul'>Jhon. Rental</h1>
              <p className='text-sub-footer col-lg-9'>Rental Mobil Berkelas Hanya Di Jhon Rental,
                Berbagai Macam Pilihan Mobil Tersedia Disini. Anda Dapat Memilih Mobil Sesuai Keinginan</p>
            </div>
            <div className='col-sm-4 col-md-3 col-lg-3'>
              <h1 className='text-footer-judul'>Contact</h1>
              <p className='text-sub-footer col-lg-9'>Rental Mobil Berkelas Hanya Di Jhon Rental,
                Berbagai Macam Pilihan Mobil Tersedia Disini. Anda Dapat Memilih Mobil Sesuai Keinginan</p>
            </div>
            <div className='col-sm-4 col-md-3 col-lg-3'>
              <h1 className='text-footer-judul'>Tentang Kami</h1>
              <p className='text-sub-footer col-lg-9'>Rental Mobil Berkelas Hanya Di Jhon Rental,
                Berbagai Macam Pilihan Mobil Tersedia Disini. Anda Dapat Memilih Mobil Sesuai Keinginan</p>
            </div>
            <div className='col-sm-4 col-md-3 col-lg-3'>
              <h1 className='text-footer-judul'>Profile</h1>
              <p className='text-sub-footer col-lg-9'>Rental Mobil Berkelas Hanya Di Jhon Rental,
                Berbagai Macam Pilihan Mobil Tersedia Disini. Anda Dapat Memilih Mobil Sesuai Keinginan</p>
            </div>
          </div>
        </div>
      </div>
    </div>
   </>
  )
}
