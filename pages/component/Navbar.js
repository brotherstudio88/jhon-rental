import React from 'react'
import { useRouter } from 'next/router'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser } from '@fortawesome/free-solid-svg-icons'
export default function Navbar() {
  const router = useRouter()
  return (
    <nav className="navbar navbar-expand-lg bg-light">
      <div className="container">
    <a className="navbar-brand" onClick={() => router.push('/')}><span className='text-span'>Jhon.</span> Rental</a>
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarNav">
      <ul className="navbar-nav">
      <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
           <FontAwesomeIcon icon={faUser} />
          </a>
          <ul class="dropdown-menu">
            <li><a className="dropdown-item" onClick={() => router.push('/user/Registrasi')} style={{cursor:'pointer'}}>Sign In</a></li>
            <li><a className="dropdown-item" onClick={() => router.push('/user/Login')} style={{cursor:'pointer'}}>Login</a></li>
          </ul>
        </li>
        <li className="nav-item">
          <a className="nav-link active" aria-current="page" href="#">Home</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#">Features</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#">Pricing</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
  )
}
