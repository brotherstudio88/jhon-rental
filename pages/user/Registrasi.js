import React from 'react'
import Image from 'next/image'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons'
import { useRouter } from 'next/router'
import Link from 'next/link'
export default function Registrasi() {
    const router = useRouter()
  return (
    <div className='registrasi-wraper'>
      <div className='container'>
        <div className='row'>
            <div className='col-sm-12 col-md-6 col-lg-6'>
                <div className='registrasi-image-wraper'>
                    <Image className='d-md-block d-none' src='/image/expander.png' width={1200} height={800} />
                    <button className='button-registrasi-kembali mt-3' onClick={() => router.push('/')}
                    >
                    <FontAwesomeIcon icon={faAngleLeft} />
                    </button>
                </div>
            </div>
            <div className='col-sm-12 col-md-6 col-lg-6'>
                <div className='card-registrasi-wraper'>
                    <div className='card-registrasi'>
                        <h1 className='registrasi-text-judul text-center'>Registrasi</h1>
                    <form>
                        <div className='registrasi-form-wraper mb-4'>
                                <div className='col-sm-12 col-md-12 col-lg-12'>
                                    <label className='text-label'>Nama Lengkap</label>
                                    <input type="text"  placeholder="Nama Lengkap..." />
                                </div>
                                <div className='col-sm-12 col-md-12 col-lg-12'>
                                    <label className='text-label'>Email</label>
                                    <input type="text"  placeholder="Email..." />
                                </div>
                                <div className='col-sm-12 col-md-12 col-lg-12'>
                                    <label className='text-label'>Password</label>
                                    <input type="password"  placeholder="Password..." />
                                </div>
                                <div className='d-flex mt-4'>
                                    <button className='button-registrasi'>Submit</button>
                                    <p className='mt-4 px-2'>Sudah Punya Akun?</p>
                                    <p className='mt-4 '><Link href='Login'>Login</Link></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  )
}
