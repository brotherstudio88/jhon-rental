import React from 'react'
import Image from 'next/image'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons'
import { useRouter } from 'next/router'
import Link from 'next/link'
export default function Login() {
    const router = useRouter()
  return (
    <div className='login-wraper'>
      <div className='container'>
        <div className='row'>
            <div className='col-sm-12 col-md-6 col-lg-6'>
                <div className='login-image-wraper'>
                    <Image className='d-md-block d-none' src='/image/expander.png' width={1200} height={800} />
                    <button className='button-login-kembali mt-3' onClick={() => router.push('/')}
                    >
                    <FontAwesomeIcon icon={faAngleLeft} />
                    </button>
                </div>
            </div>
            <div className='col-sm-12 col-md-6 col-lg-6'>
                <div className='card-login-wraper'>
                    <div className='card-login'>
                        <h1 className='login-text-judul text-center'>Login</h1>
                    <form>
                        <div className='login-form-wraper mb-4'>
                                <div className='col-sm-12 col-md-12 col-lg-12'>
                                    <label className='text-label'>Email</label>
                                    <input type="text"  placeholder="Email..." />
                                </div>
                                <div className='col-sm-12 col-md-12 col-lg-12'>
                                    <label className='text-label'>Password</label>
                                    <input type="password"  placeholder="Password..." />
                                </div>
                                <div className='d-flex mt-4'>
                                    <button className='button-login'>Submit</button>
                                    <p className='mt-4 px-2'>Belum Punya Akun ?</p>
                                    <p className='mt-4 '><Link href='Registrasi'>Registrasi</Link></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  )
}
