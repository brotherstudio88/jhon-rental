import Navbar from '../component/Navbar'
import Image from 'next/image'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons'
import { useRouter } from 'next/router'
export default function index() {
    const router = useRouter()
  return (
    <>
    <Navbar />
    <div className='rental-wraper mb-4'>
        <div className='container'>
            <h1 className='rental-judul-text'>Silahkan Pilih Mobil</h1>
            <button className='button-rental-kembali mt-3' onClick={() => router.push('/')}
            >
            <FontAwesomeIcon icon={faAngleLeft} />
            </button>
                <div className='row'>
                    <div className='col-sm-6 col-md-4 col-lg-4'>
                        <div className='card-rental-mobil-wraper'>
                            <div className='card-rental-mobil' onClick={() => router.push('../pesan')}>
                                <Image src='/image/pajero.png' width={550} height={300} />
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-6 col-md-4 col-lg-4'>
                        <div className='card-rental-mobil-wraper'>
                            <div className='card-rental-mobil' onClick={() => router.push('../pesan')}>
                                <Image src='/image/alphard.png' width={550} height={300} />
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-6 col-md-4 col-lg-4'>
                        <div className='card-rental-mobil-wraper'>
                            <div className='card-rental-mobil' onClick={() => router.push('../pesan')}>
                                <Image src='/image/expander.png' width={550} height={300} />
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-6 col-md-4 col-lg-4'>
                        <div className='card-rental-mobil-wraper'>
                            <div className='card-rental-mobil' onClick={() => router.push('../pesan')}>
                                <Image src='/image/alphard.png' width={550} height={300} />
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-6 col-md-4 col-lg-4'>
                        <div className='card-rental-mobil-wraper'>
                            <div className='card-rental-mobil' onClick={() => router.push('../pesan')}>
                                <Image src='/image/expander.png' width={550} height={300} />
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-6 col-md-4 col-lg-4'>
                        <div className='card-rental-mobil-wraper'>
                            <div className='card-rental-mobil' onClick={() => router.push('../pesan')}>
                                <Image src='/image/pajero.png' width={550} height={300} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </>
  )
}
