import React from 'react'
import Navbar from '../component/Navbar'
import Image from 'next/image'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons'
import { useRouter } from 'next/router'
export default function index() {
    const router = useRouter()
  return (
    <>
    <Navbar />
    <div className='pesan-wraper'>
        <div className='container'>
            <h1 className='pesan-judul-text'>Cek Out Pesanan</h1>
            <button className='button-pesan-kembali mt-3' onClick={() => router.push('../rental')}
            >
            <FontAwesomeIcon icon={faAngleLeft} />
            </button>
                <div className='row'>
                    <div className='col-sm-6 col-md-6 col-lg-6'>
                        <div className='card-pesan-mobil-wraper'>
                            <div className='card-pesan-mobil'>
                            <Image src='/image/pajero.png' width={700} height={400} />
                            </div>
                        </div>
                    </div>
                    <div className='col-sm-6 col-md-6 col-lg-6'>
                        <div className='card-form-pesan-wraper'>
                            <div className='card-form-pesan'>
                                <h1 className='text-judul-form text-center'>Pendataan Peminjam</h1>
                                <p className='text-sub-form text-center'>Silahkan Isi Form Di Bawah</p>
                                <form>
                                    <div className='wraper-form'>
                                        <div className='row'>
                                            <div className='col-sm-12 col-md-12 col-lg-12'>
                                                <label className='text-label'>Nama Lengkap</label>
                                                <input type="text"  placeholder="Nama Lengkap..." />
                                            </div>
                                            <div className='col-sm-6 col-md-6 col-lg-6'>
                                                <label className='text-label'>Nama Mobil</label>
                                                <select>
                                                    <option selected="true" disabled="disabled">Pilih Nama Mobil</option>
                                                    <option value="Lainnya">Alphard</option>
                                                    <option value="Lainnya">Pajero Sport</option>
                                                    <option value="Lainnya">Expander</option>
                                                    <option value="Lainnya">Civic Turbo</option>
                                                    <option value="Lainnya">Fortuner</option>
                                                    <option value="Lainnya">Brio</option>
                                                    <option value="Lainnya">Jazz</option>
                                                </select>
                                            </div>
                                            <div className='col-sm-6 col-md-6 col-lg-6'>
                                                <label className='text-label'>Jenis Jaminan</label>
                                                <select>
                                                    <option selected="true" disabled="disabled">Plihin Jenis Jaminan</option>
                                                    <option value="Lainnya">KTP</option>
                                                    <option value="Lainnya">Lainnya</option>
                                                </select>
                                            </div>
                                            <div className='col-sm-6 col-md-6 col-lg-6'>
                                                <label className='text-label'>Tanggal Peminjaman</label>
                                                <input type="date"/>
                                            </div>
                                            <div className='col-sm-6 col-md-6 col-lg-6'>
                                                <label className='text-label'>Tanggal Kembali</label>
                                                <input type="date"/>
                                            </div>
                                            <div className='col-sm-12 col-md-12 col-lg-12'>
                                                <label className='text-label'>Bukti Pembayaran</label>
                                                <input class="form-control" type="file"/>
                                            </div>
                                            <div className='col-sm-12 col-md-12 col-lg-12'>
                                                <label className='text-label'>Alamat</label>
                                                <textarea type="textarea" placeholder='Alamat...' rows={5}/>
                                            </div>
                                        </div>
                                        <button className='button-pesan-submit'>Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </>
  )
}
